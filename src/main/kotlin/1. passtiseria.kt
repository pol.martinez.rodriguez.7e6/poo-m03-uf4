
fun main () {
    val croissant = Pasta("Croissant", 1.6,0.3,45)
    val ensaimada = Pasta("Ensaimada", 0.8,0.2,120)
    val donut = Pasta("Donut",0.6,0.4,80 )
    val pastas = mutableListOf(croissant, ensaimada, donut)
    val aigua = Beguda("Aigua", 1.00, false)
    val cafeTallat = Beguda("Café tallat", 1.35, false)
    val teVermell = Beguda("Té vermell", 1.50, false)
    val cola = Beguda("Cola", 1.65, true)
    val begudes = mutableListOf(aigua, cafeTallat, teVermell, cola)
    for (i in pastas){
        println("- ${i.nom}: preu:${i.preu}€ pes: ${i.pes}Kg calorias: ${i.calorias}Kcal")
    }
    for (i in begudes){
        println("- ${i.nom}: preu:${i.preu}€ ")
    }
}