import java.util.Scanner

class Fusteria {
    data class Taulell( val preuUnitari:  Int, val llargada : Int, val amplada : Int)
    data class Llisto( val preuMetre:  Int, val metres : Int)

    fun calculatePreuTaulell(preuUnitari: Int, llargada: Int, amplada: Int): Int {
        val taulell = Taulell(preuUnitari, llargada, amplada)
        val area = taulell.amplada * taulell.llargada
        return area * preuUnitari
    }
    fun calculatePreuLlisto(preuMetre: Int,metres: Int) : Int {
        val llisto = Llisto(preuMetre, metres)
        return llisto.preuMetre * llisto.metres
    }
}
fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix el nombre de productes:")
    val numArticles = scanner.nextInt()
    var preuTotal = 0
    for (i in 0 until numArticles){
        val tipusArticle = scanner.next().uppercase()
        if (tipusArticle != "TAULELL"){
            val preuMetre = scanner.nextInt()
            val metres = scanner.nextInt()
            preuTotal += Fusteria().calculatePreuLlisto(preuMetre, metres)
        }
        else {
            val preuUnitari = scanner.nextInt()
            val llargada = scanner.nextInt()
            val amplada = scanner.nextInt()
            preuTotal += Fusteria().calculatePreuTaulell(preuUnitari, llargada, amplada)
        }
    }
    println("El preu total és: $preuTotal€")

}