class Beguda
    (val nom : String,
     var preu : Double,
     val sucre : Boolean){
        init {
            if (sucre) preu += (preu * 0.1)
        }
}