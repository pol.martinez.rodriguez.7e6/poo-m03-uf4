class MechanicalArm () {
    var estat = false //true encés /fals apagat
    var apertura = 0
    var height = 0
    fun toggle() {
        estat = !estat
        println("MechanicalArm{openAngle=$apertura, altitude=$height, turnedOn=${estat}")
    }
    fun updateHeight (newHeight : Int) {
        if (estat){
            if ((newHeight + height) in 0 ..30) height += newHeight
        }
        println("MechanicalArm{openAngle=$apertura, altitude=$height, turnedOn=${estat}")
    }
    fun updateApertura (newAngle: Int) {
        if (estat){
            if ((newAngle + apertura) in 0 ..360) apertura += newAngle
        }
        println("MechanicalArm{openAngle=$apertura, altitude=$height, turnedOn=${estat}")
    }
}
fun main () {
    val brazo = MechanicalArm()
    brazo.toggle()
    brazo.updateHeight(3)
    brazo.updateApertura(180)
    brazo.updateHeight(-3)
    brazo.updateApertura(-180)
    brazo.updateHeight(3)
    brazo.toggle()
}