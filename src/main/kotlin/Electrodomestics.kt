import java.lang.reflect.Type

open class Electrodomestics (var preu: Int , var weight : Int = 5, var consum : String = "G", var color :  String = "blanc") {
    var preuFinal = 0
    private var relacioConsumPreu = mutableListOf("A-35", "B-30","C-25","D-20","E-15","F-10","G-0")
    private var relacioPesPreu = mutableListOf("6..20-20","21..50-50","51..80-80",">80-100")
    init {
        preuFinal()
    }
    open fun preuFinal () {
        preuFinal += preu
        for (i in relacioConsumPreu){
            if (consum == i.split("-")[0]) preuFinal += i.split("-")[1].toInt()
        }
        for (i in relacioPesPreu){
            if (i == ">80-100"){
                if (weight > 80) preuFinal += i.split("-")[1].toInt()
            }
            else{
                val firstNumero = i.split("-")[0].split("..")[0].toInt()
                val segonNumero = i.split("-")[0].split("..")[1].toInt()
                if (weight in firstNumero .. segonNumero) preuFinal += i.split("-")[1].toInt()
            }
        }
    }
}
class Rentadora (var preuRent : Int , var car : Int = 5, var peso: Int,val cons: String, var col: String): Electrodomestics(preuRent, peso, cons, col) {
    init {
        super.preuFinal
        preuFinal()
    }
     override fun preuFinal() {
         super.preuFinal += preuRent
         for (i in mutableListOf("6-55","7-55","8-70","9-85", "10-100")){
             if (car == i.split("-")[0].toInt()) super.preuFinal += i.split("-")[1].toInt()
         }
    }
}
class  Televisor (var preuTele: Int, var size : Int = 28, var peso: Int,val cons: String, var col: String): Electrodomestics(preuTele, peso, cons, col) {
    init {
        super.preuFinal
        preuFinal()
    }
    override fun preuFinal() {
        super.preuFinal += preuTele
        for (i in mutableListOf("29..32-50","33..42-100", "43..50-150", ">50-200")){
            if (i == ">50-200"){
                if (size > 50) super.preuFinal += i.split("-")[1].toInt()
            }
            else{
                val firstNumero = i.split("-")[0].split("..")[0].toInt()
                val segonNumero = i.split("-")[0].split("..")[1].toInt()
                if (size in firstNumero .. segonNumero) super.preuFinal += i.split("-")[1].toInt()
            }
        }
    }
}
fun main () {
    val elec1 = Electrodomestics(35,2,"D","blanc")
    val elec2 = Electrodomestics(14,10,"A","platejat")
    val elec3 = Electrodomestics(20,6,"C","blanc")
    val elec4 = Electrodomestics(40,3,"B","color")
    val elec5 = Electrodomestics(50,15,"F", "blanc")
    val elec6 = Electrodomestics(60,20,"G", "blanc")
    val rent1 = Rentadora(30,5,10,"A","blanc")
    val rent2 = Rentadora(20,8,20,"F","blanc")
    val tele1 = Televisor(30,52,40,"B", "blanc")
    val tele2 = Televisor(20,29,20,"D", "blanc")
    val electrodomestics = mutableListOf(elec1,elec2,elec3,elec4,elec5,elec6,rent1,rent2,tele1,tele2)
    val precios = MutableList(6){0}
    for ( i in electrodomestics){
        if (i is Rentadora || i is Televisor) {
            if (i is Rentadora) {
                println("- Rentadora:")
                precios[2] += i.preu
                precios[3] += i.preuFinal
            }
            else {
                println("- Televisor:")
                precios[4] += i.preu
                precios[5] += i.preuFinal
            }
            println("\t- Preu Base : ${i.preu}")
            println("\t- Preu Final : ${i.preuFinal}")
        }
        else{
            println("- Electrodomestic:\n" +
                    "\t- Preu Base : ${i.preu}€\n" +
                    "\t- Color : ${i.color}\n" +
                    "\t- Consum : ${i.consum}\n" +
                    "\t- Pes : ${i.weight}kg\n" +
                    "\t- Preu Final : ${i.preuFinal}€")
            precios[0] += i.preu
            precios[1] += i.preuFinal
        }
    }
    println("Electrodomestics: \n" +
            "\t -  Preu Base : ${precios[0]}€\n" +
            "\t -  Preu Final : ${precios[1]}€\n" +
            "Rentadores: \n" +
            "\t -  Preu Base : ${precios[2]}€\n" +
            "\t -  Preu Final : ${precios[3]}€\n" +
            "Televisors: \n" +
            "\t -  Preu Base : ${precios[4]}€\n" +
            "\t -  Preu Final : ${precios[5]}€\n")

}